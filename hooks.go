package log

import (
	"time"

	"github.com/Abramovic/logrus_influxdb"
	"github.com/Sirupsen/logrus"
	"github.com/deferpanic/deferclient/deferstats"
	"github.com/deferpanic/dp-logrus/middleware"
	"github.com/evalphobia/logrus_sentry"
	"github.com/johntdyer/slackrus"
	"github.com/spf13/viper"
)

var verboseLoggerInit = false

func AddHookSentryLoggerHook(logger *logrus.Logger) {
	if viper.IsSet("logger.sentry") && viper.IsSet("logger.sentry.dsn") {
		hook, err := logrus_sentry.NewSentryHook(viper.GetString("logger.sentry.dsn"), []logrus.Level{
			logrus.PanicLevel,
			logrus.FatalLevel,
			logrus.ErrorLevel,
			//log.WarnLevel,
			//log.InfoLevel,
			//log.DebugLevel,
		})
		if err == nil {
			hook.StacktraceConfiguration.Enable = true
			hook.StacktraceConfiguration.Context = 3
			hook.Timeout = 20 * time.Second
			if verboseLoggerInit {
				logger.Debug("Adding sentry log hook")
			}
			logger.Hooks.Add(hook)
		} else {
			logger.WithField("service", "sentry").Warn(err)
		}
	}
}

func AddHookInfluxDBLoggerHook(logger *logrus.Logger) {
	if viper.IsSet("logger.influxdb") && viper.IsSet("logger.influxdb.address") {
		if hook, err := logrus_influxdb.NewInfluxDBHook(
			viper.GetString("logger.influxdb.address"),
			viper.GetString("logger.influxdb.database"),
			nil,
		); err == nil {
			if verboseLoggerInit {
				logger.Debug("Adding influxdb log hook")
			}
			logger.Hooks.Add(hook)
		}
	}
}

func AddDeferPanicLoggerHook(logger *logrus.Logger) {

	if viper.IsSet("logger.deferpanic") && viper.IsSet("logger.deferpanic.api_key") {
		dps := deferstats.NewClient(viper.GetString("logger.deferpanic.api_key"))

		go dps.CaptureStats()

		if hook, err := middleware.NewDPHook(dps); err == nil {
			if verboseLoggerInit {
				logger.Debug("Adding deferpanic log hook")
			}
			logger.Hooks.Add(hook)
		}
	}
}

func AddSlackLoggerHook(logger *logrus.Logger) {
	if viper.IsSet("logger.slack") && viper.IsSet("logger.slack.url") {
		viper.SetDefault("logger.slack.channel", "logs")
		if viper.GetBool("debug") {
			viper.SetDefault("logger.slack.level", "debug")
		} else {
			viper.SetDefault("logger.slack.level", "info")
		}
		viper.SetDefault("logger.slack.emoji", ":page_facing_up:")
		viper.SetDefault("logger.slack.username", "app")
		level, err := logrus.ParseLevel(viper.GetString("logger.slack.level"))
		if err != nil {
			logrus.WithError(err).Info("Invalid level for config field \"logger.slack.level\"")
		} else {
			if verboseLoggerInit {
				logger.Debug("Adding slack log hook")
			}
			logger.Hooks.Add(&slackrus.SlackrusHook{
				HookURL:        viper.GetString("logger.slack.url"),
				AcceptedLevels: slackrus.LevelThreshold(level),
				Channel:        viper.GetString("logger.slack.channel"),
				IconEmoji:      viper.GetString("logger.slack.emoji"),
				Username:       viper.GetString("logger.slack.username"),
			})
		}
	}
}
