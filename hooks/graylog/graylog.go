package graylog

import (
	"github.com/Sirupsen/logrus"
	graylog "gopkg.in/gemnasium/logrus-graylog-hook.v2"
)

func Hook(address string, log *logrus.Logger, packageName string) {
	hook := graylog.NewGraylogHook(address, map[string]interface{}{
		"package": packageName,
	})
	log.Hooks.Add(hook)
	// if err := hook.Fire(logrus.WithField("graylog_address", address)); err != nil {
	// 	panic(err)
	// }
}
