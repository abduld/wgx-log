package log

import (
	"runtime"
	"strings"

	"github.com/Sirupsen/logrus"
	"github.com/x-cray/logrus-prefixed-formatter"
	"gitlab.com/abduld/wgx-log/hooks/graylog"
)

type Logger struct {
	*logrus.Logger
}

var GraylogAddress = "logs.webgpu.com:12202"

var (
	// std is the name of the standard logger in stdlib `log`
	std       *Logger
	level     logrus.Level
	formatter logrus.Formatter
)

func New() *Logger {
	packageName := getPackageName(2)

	logger := logrus.New()
	logger.Level = level
	logger.Formatter = formatter
	graylog.Hook(GraylogAddress, logger, packageName)
	return &Logger{Logger: logger}
}

func NewWithConfig(lvl logrus.Level, fmt logrus.Formatter) *Logger {
	level = lvl
	if fmt == nil {
		formatter = StandardFormatter()
	} else {
		formatter = fmt
	}
	return New()
}
func StandardLogger() *Logger {
	return std
}

func SetStandardLogger(newStd *Logger) {
	std = newStd
}

func StandardFormatter() logrus.Formatter {
	return &VerboseTextFormatter{
		ShowLocation: true,
		TextFormatter: prefixed.TextFormatter{
			ForceColors:    true,
			ShortTimestamp: false,
		},
	}
}

func SetStandardFormatter(frm logrus.Formatter) {
	formatter = frm
}

func (log *Logger) Log(args ...interface{}) {
	log.Debug(args...)
}

func getPackageName(skip int) string {
	pc, _, _, _ := runtime.Caller(skip)
	fullName := runtime.FuncForPC(pc).Name()
	parts := strings.Split(fullName, ".")

	packageName := strings.Join(parts[0:2], ".")
	return strings.TrimPrefix(packageName, "gitlab.com/abduld/")
}

func init() {
	level = logrus.DebugLevel
	formatter = StandardFormatter()
	std = New()
}
