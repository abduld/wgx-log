package log

import (
	"bytes"
	"encoding/json"
	"fmt"
	"go/build"
	"path/filepath"
	"reflect"
	"runtime"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/bugsnag/osext"
	"github.com/facebookgo/stack"
	"github.com/mgutz/ansi"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"
)

const (
	reset          = ansi.Reset
	stackSkipCount = 7
)

var (
	baseTimestamp time.Time
	isTerminal    bool
	exeFolder     string
	pkgPath       string
	trimPaths     []string
)

func init() {
	type Empty struct{}
	baseTimestamp = time.Now()
	isTerminal = logrus.IsTerminal()
	exeFolder, _ = osext.ExecutableFolder()
	pkgPath = strings.TrimRight(reflect.TypeOf(Empty{}).PkgPath(), "utils")

	// Collect all source directories, and make sure they
	// end in a trailing "separator"
	for _, prefix := range build.Default.SrcDirs() {
		if prefix[len(prefix)-1] != filepath.Separator {
			prefix += string(filepath.Separator)
		}
		trimPaths = append(trimPaths, prefix)
	}
}

func miniTS() int {
	return int(time.Since(baseTimestamp) / time.Second)
}

type VerboseTextFormatter struct {
	prefixed.TextFormatter
	ShowLocation bool
}

// Try to trim the GOROOT or GOPATH prefix off of a filename
func trimPath(filename string) string {
	for _, prefix := range trimPaths {
		if trimmed := strings.TrimPrefix(filename, prefix); len(trimmed) < len(filename) {
			return trimmed
		}
	}
	return filename
}

func (f *VerboseTextFormatter) getLineInfo() string {
	trimFile := func(file string) string {
		if strings.HasPrefix(file, pkgPath) {
			return file[len(pkgPath):]
		} else if strings.HasPrefix(file, exeFolder) {
			return file[len(exeFolder):]
		} else if strings.HasPrefix(file, "github.com") {
			return "gh" + file[len("github.com"):]
		} else if strings.HasPrefix(file, "gitlab.com") {
			return "gl" + file[len("gitlab.com"):]
		}
		return trimPath(file)
	}

	isColorTerminal := isTerminal && (runtime.GOOS != "windows")
	isColored := (f.ForceColors || isColorTerminal) && !f.DisableColors

	frame := stack.Caller(stackSkipCount)

	file := trimFile(frame.File)
	if file == "" {
		return ""
	}
	line := strconv.Itoa(frame.Line)
	name := frame.Name
	if isColored {
		file = ansi.Red + file + reset
		line = ansi.Red + line + reset
		name = ansi.Blue + name + reset
		res := "[" + file + "::" + name + "::" + line + "]"
		return res
	} else {
		return fmt.Sprintf("%s:%s %s", file, line, name)
	}
}

func getStackInformation() string {
	currentStacktrace := stack.Caller(stackSkipCount)

	if js, err := json.Marshal(currentStacktrace); err == nil {
		return string(js)
	}
	return ""
}

func (f *VerboseTextFormatter) Format(entry *logrus.Entry) ([]byte, error) {

	if entry.Level <= logrus.ErrorLevel {
		entry.Data["stack_trace"] = getStackInformation()
	}

	var keys []string = make([]string, 0, len(entry.Data))
	for k := range entry.Data {
		if k != "prefix" {
			keys = append(keys, k)
		}
	}
	if f.ShowLocation {
		entry.Data["location"] = f.getLineInfo()
	}

	if !f.DisableSorting {
		sort.Strings(keys)
	}

	b := &bytes.Buffer{}

	prefixFieldClashes(entry.Data)

	isColorTerminal := isTerminal && (runtime.GOOS != "windows")
	isColored := (f.ForceColors || isColorTerminal) && !f.DisableColors

	timestampFormat := f.TimestampFormat
	if timestampFormat == "" {
		timestampFormat = time.Stamp
	}
	if isColored {
		f.printColored(b, entry, keys, timestampFormat)
	} else {
		if !f.DisableTimestamp {
			f.appendKeyValue(b, "time", entry.Time.Format(timestampFormat))
		}

		if f.ShowLocation {
			f.appendKeyValue(b, "location", entry.Data["location"])
		}
		f.appendKeyValue(b, "level", entry.Level.String())
		if entry.Message != "" {
			f.appendKeyValue(b, "msg", entry.Message)
		}
		for _, key := range keys {
			f.appendKeyValue(b, key, entry.Data[key])
		}
	}

	b.WriteByte('\n')
	return b.Bytes(), nil
}

func (f *VerboseTextFormatter) printColored(b *bytes.Buffer, entry *logrus.Entry, keys []string, timestampFormat string) {
	var levelColor string
	var levelText string
	switch entry.Level {
	case logrus.InfoLevel:
		levelColor = ansi.Green
	case logrus.WarnLevel:
		levelColor = ansi.Yellow
	case logrus.ErrorLevel, logrus.FatalLevel, logrus.PanicLevel:
		levelColor = ansi.Red
	default:
		levelColor = ansi.Blue
	}

	if entry.Level != logrus.WarnLevel {
		levelText = strings.ToUpper(entry.Level.String())
	} else {
		levelText = "WARN"
	}

	prefix := ""
	prefixValue, ok := entry.Data["prefix"]
	if ok {
		prefix = fmt.Sprint(" ", ansi.Cyan, prefixValue, ":", reset)
	}

	loc := " | "
	if f.ShowLocation {
		if e, ok := entry.Data["location"]; ok && e != "" {
			loc = loc + fmt.Sprint(e) + " | "
		}
	}
	if f.ShortTimestamp {
		fmt.Fprintf(b, "%s[%04d]%s %s%+5s%s%s%s%s", ansi.LightBlack, miniTS(), reset, levelColor, levelText, reset, prefix, loc, entry.Message)
	} else {
		fmt.Fprintf(b, "%s[%s]%s %s%+5s%s%s%s%s", ansi.LightBlack, entry.Time.Format(timestampFormat), reset, levelColor, levelText, reset, prefix, loc, entry.Message)
	}
	for _, k := range keys {
		v := entry.Data[k]
		fmt.Fprintf(b, " %s%s%s=%+v", levelColor, k, reset, v)
	}
}

func needsQuoting(text string) bool {
	for _, ch := range text {
		if !((ch >= 'a' && ch <= 'z') ||
			(ch >= 'A' && ch <= 'Z') ||
			(ch >= '0' && ch <= '9') ||
			ch == '-' || ch == '.') {
			return false
		}
	}
	return true
}

func (f *VerboseTextFormatter) appendKeyValue(b *bytes.Buffer, key string, value interface{}) {
	b.WriteString(key)
	b.WriteByte('=')

	switch value := value.(type) {
	case string:
		if needsQuoting(value) {
			b.WriteString(value)
		} else {
			fmt.Fprintf(b, "%q", value)
		}
	case error:
		errmsg := value.Error()
		if needsQuoting(errmsg) {
			b.WriteString(errmsg)
		} else {
			fmt.Fprintf(b, "%q", value)
		}
	default:
		fmt.Fprint(b, value)
	}

	b.WriteByte(' ')
}

func prefixFieldClashes(data logrus.Fields) {
	_, ok := data["location"]
	if ok {
		data["fields.location"] = data["location"]
	}
	_, ok = data["time"]
	if ok {
		data["fields.time"] = data["time"]
	}
	_, ok = data["msg"]
	if ok {
		data["fields.msg"] = data["msg"]
	}
	_, ok = data["level"]
	if ok {
		data["fields.level"] = data["level"]
	}
}
